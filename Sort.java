import java.util.*;
public class Sort
{
	public static void main(String[] args) {
		int t=0,i,j;
		Scanner x=new Scanner(System.in);
		System.out.println("Enter the no. of elements in array:");
		int n=x.nextInt();
		int a[]=new int[n];
		System.out.println("Enter the elements: ");
		for(i=0;i<n;i++){
		    a[i]=x.nextInt();
		}
		for(i=0;i<n;i++){
		    for(j=i+1;j<n;j++){
		        if(a[i]>a[j]){
		            t=a[i];
		            a[i]=a[j];
		            a[j]=t;
		            }
			
		        }
		    }
		     System.out.println("Sorted array in asc. order:");
		    for(i=0;i<n;i++){
		        System.out.print(a[i]+" ");
			}
			
	}
}

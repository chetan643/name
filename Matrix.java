import java.util.*;
public class Matrix
{
	public static void main(String[] args) {
		Scanner g=new Scanner(System.in);
		int r,c,i,j,k=0,l=0,m=0,n=0;
		System.out.print("Enter the no. of rows and columns:");
		r=g.nextInt();
		c=g.nextInt();
		int ar[][]=new int[r][c];
		System.out.print("Enter elements of matrix:");
		for(i=0;i<r;i++){
		    for(j=0;j<c;j++){
		        ar[i][j]=g.nextInt();
		    }
		}
		for(i=0;i<r;i++){
		    for(j=0;j<c;j++){
		        System.out.print(ar[i][j]+" ");
		        if(i==j)
		            k=k+ar[i][j];
		        if((i+j)==(r-1))
		            l=l+ar[i][j];
		        if(i!=2&&j!=0&i!=j)
		            m=m+ar[i][j];
		        if(i!=0&&j!=2&&i!=j)
		            n=n+ar[i][j];
		    }
		    System.out.println();
		}
		System.out.println("sum of 1st diagonal elements is:"+k);
		System.out.println("Sum of 2nd diagonal elements is:"+l);
		System.out.println("Sum of upper triangular elements is:"+m);
		System.out.println("Sum of lower triangular elements is:"+n);
	}
}
